var app = require('express')();
var bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded



app.post('/GetSpeakerTestReport', function (req, res) {
  console.log(req.body);
  res.status(200).json({
    SpeakerTestReport: {
      SpeakerTestStatus: "OK",
      TestTimestamp: "2015-06-10T16:38:45Z",
      CalibrationTimestamp: "2015-05-28T16:30:02Z"
    }
  });
  console.log(res.body);
  res.end();
});

// estatus pending
app.post('/GetSpeakerTestReportPending', function (req, res) {
  // console.log(req.body);
  res.status(200).json({
    SpeakerTestReport: {
      SpeakerTestStatus: "Pending",
      TestTimestamp: "",
      CalibrationTimestamp: ""
    }
  });
  console.log(res.body);
  res.end();
});

// estatus calibrated
app.post('/GetSpeakerTestReportCalibrated', function (req, res) {
  // console.log(req.body);
  res.status(200).json({
    SpeakerTestReport: {
      SpeakerTestStatus: "Calibrated",
      TestTimestamp: "",
      CalibrationTimestamp: "2015-05-28T16:30:02Z"
    }
  });
  console.log(res.body);
  res.end();
});


// estatus calibrated
app.post('/GetSpeakerTestReportUncalibrated', function (req, res) {
  // console.log(req.body);
  res.status(200).json({
    SpeakerTestReport: {
      SpeakerTestStatus: "Uncalibrated",
      TestTimestamp: "",
      CalibrationTimestamp: "2015-05-28T16:30:02Z"
    }
  });
  console.log(res.body);
  res.end();
});

// Failed
app.post('/GetSpeakerTestReportFailed', function (req, res) {
  // console.log(req.body);
  res.status(404).json({
    SpeakerTestReport: {
      SpeakerTestStatus: "Failed",
      TestTimestamp: "",
      CalibrationTimestamp: ""
    }
  });
  console.log(res.body);
  res.end();
});



app.post('/GetSpeakerTestDeviceNotCalibrated', function (req, res) {
  // console.log(req.body);
  res.status(400).json({

    Fault: "env:Receiver",
    FaultCode: "ter:Action",
    FaultSubCode: "axast:DeviceNotCalibrated",
    FaultReason: "The Auto Speaker Test cannot be done without prior calibration.",
    FaultMsg : null 

  });
  console.log(res.body);
  res.end();
});

// error 500 server internal error 

app.post('/GetSpeakerTestInternalError', function (req, res) {
  // console.log(req.body);
  res.status(500).json({

    Fault: "env:Receiver",
    FaultCode: "ter:Action",
    FaultSubCode: "axast:InternalError",
    FaultReason: " La prueba del altavoz falló debido a un error interno. Consulte el registro del sistema para obtener información.",
    FaultMsg : null 
  });
  console.log(res.body);
  res.end();
});

app.listen(3000);
